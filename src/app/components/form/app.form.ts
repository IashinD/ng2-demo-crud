import { Component } from '@angular/core';

import { Property } from '../../models';

@Component({
  selector: 'app-form',
  templateUrl: 'app.form.html',
  styleUrls: ['app.form.scss']
})
export class FormComponent {

  model = new Property();

  onFormSubmitted() {

  }

}
