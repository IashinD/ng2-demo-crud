import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './components/list';
import { FormComponent } from './components/form';
import { DetailsComponent } from './components/details';

const appRoutes: Routes = [
  { path: 'estates/:id', component: DetailsComponent },
  { path: 'estates', component: ListComponent },
  { path: 'form', component: FormComponent },
  { path: '', redirectTo: '/estates', pathMatch: 'full' }
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
