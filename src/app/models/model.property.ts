import { Address } from './model.address';

type PropertyType = 'flat' | 'house' | 'room';
type DealType = 'sell' | 'buy' | 'rent' | 'rent out'

export class Property {
  propertyType: PropertyType;
  dealType: DealType;

  dealTypes: ArrayLike<string> = ['sell', 'buy', 'rent out'];
  propertyTypes: ArrayLike<string> = ['flat', 'house', 'room'];

  address:Address;

  constructor() {
    this.address = new Address();
  }
}
