import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { FormComponent } from './components/form';
import { DetailsComponent } from './components/details';
import { ListComponent } from './components/list';
import { HeaderComponent } from './components/header/';

import { routing, appRoutingProviders } from './app.routing';

import { CapitalizePipe, CapitalizeAllPipe } from './pipes';

@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    DetailsComponent,
    ListComponent,
    HeaderComponent,
    CapitalizePipe,
    CapitalizeAllPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [ appRoutingProviders ],
  bootstrap: [AppComponent]
})
export class AppModule { }
